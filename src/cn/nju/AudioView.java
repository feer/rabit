package cn.nju;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class AudioView extends View {
	Paint paint;
	boolean audio_on = false;
	boolean click = false;
	public AudioView(Context context) {
		super(context);
		paint = new Paint();
	}

	public void onDraw(Canvas canvas){
		super.onDraw(canvas);
		canvas.drawColor(Color.BLACK);
		paint.setTextSize(20);
		paint.setColor(Color.WHITE);
		canvas.drawText("是否打开音效?", 125, 85, paint);
		if(click){
			if(audio_on){
				paint.setColor(Color.RED);
				canvas.drawText("是", 33, 197, paint);
				paint.setColor(Color.WHITE);
				canvas.drawText("否", 336, 197, paint);
			}else{
				paint.setColor(Color.WHITE);
				canvas.drawText("是", 33, 197, paint);
				paint.setColor(Color.RED);
				canvas.drawText("否", 336, 197, paint);
			}
		}else {
				paint.setColor(Color.WHITE);
				canvas.drawText("是", 33, 197, paint);
				canvas.drawText("否", 336, 197, paint);
		}
	}

	public boolean isAudio_on() {
		return audio_on;
	}

	public void setAudio_on(boolean audioOn) {
		audio_on = audioOn;
	}

	public boolean isClick() {
		return click;
	}

	public void setClick(boolean click) {
		this.click = click;
	}
}
