package cn.nju;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

public class HelpView extends View{
	private Bitmap bg;
	private Paint paint;
	
	public HelpView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		paint = new Paint();
		bg = BitmapFactory.decodeResource(getResources(), R.drawable.help);
	}
	
	protected void onDraw(Canvas canvas){
		canvas.drawBitmap(bg, 0, 0, paint);
	}

}
