package cn.nju;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

public class GameActivity extends Activity {
	private GameSurfaceView gameSurfaceView;
	private boolean audio_on = true;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		audio_on = this.getIntent().getBooleanExtra("audio", true);
		gameSurfaceView = new GameSurfaceView(this);
		initWindow();
		this.setContentView(gameSurfaceView);
		gameSurfaceView.requestFocus();
	}

	public void initWindow(){
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		System.out.println("game activity key up");
		gameSurfaceView.onKeyUp(keyCode, event);
		if(keyCode == KeyEvent.KEYCODE_BACK){
			this.finish();
		}
		return true;
	}

	public boolean isAudio_on() {
		return audio_on;
	}

	public void setAudio_on(boolean audioOn) {
		audio_on = audioOn;
	}


	
	
}
