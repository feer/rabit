package cn.nju;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameSurfaceView extends SurfaceView implements
		SurfaceHolder.Callback {
	private Rabit rabit;
	private RefurbishThread refurbishThread;
	private List<Bell> bell_list = new ArrayList<Bell>();
	private Backgroud bg;
	private Context context;
	private BellCreator bell_creator;
	private Bird bird;
	private SurfaceHolder holder;
	private Bitmap bitmap_jump;
	private boolean jump_button_clicked = false;
	private boolean jump_comm_flag = false;
	private boolean audio_on = false;
	private AudioProvider audioProvider;
	private int score = 0;
	private int highest_score;
	private int hit_count = 0;
	private Paint paint;
	private boolean game_over = false;
	private Conclusion conclusion;

	public GameSurfaceView(Context context) {
		super(context);
		this.context = context;
		rabit = new Rabit(context);
		bg = new Backgroud(context);
		conclusion = new Conclusion(this);
		bitmap_jump = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.button);
		init_audio();
		init_highest_score();
		bell_creator = new BellCreator(context);
		bird = new Bird(context);

		refurbishThread = new RefurbishThread();
		init_bell_list();
		paint = new Paint();
		this.holder = this.getHolder();
		holder.addCallback(this);
		this.setFocusable(true);
	}

	private void init() {
		rabit.init();
		bg.init();
		init_bird();
		jump_button_clicked = false;
		jump_comm_flag = false;
		for (int i = 0; i < bell_list.size(); ++i) {
			bell_creator.recycle(bell_list.get(i));
		}
		bell_creator.init();
		init_bell_list();
		highest_score = (highest_score > score) ? highest_score : score;
		score = 0;
		hit_count = 0;
	}

	private void init_bird() {
		bird.setState(Bird.BIRD_LEFT_FLY0);
		bird.setCenter_x(240);
		bird.setCenter_y(30);
		bird.setOn_screen(true);
	}

	private void init_highest_score() {
		SharedPreferences settings = ((GameActivity) context)
				.getPreferences(Activity.MODE_PRIVATE);
		highest_score = settings.getInt("highestscore", 0);
	}

	private void init_audio() {
		this.audio_on = ((GameActivity) context).isAudio_on();
		if (audio_on) {
			audioProvider = new AudioProvider(context);
		}
	}

	private void init_bell_list() {
		Bell bell0 = bell_creator.createBell();
		Bell bell1 = bell_creator.createBell();
		Bell bell2 = bell_creator.createBell();
		Bell bell3 = bell_creator.createBell();
		Bell bell4 = bell_creator.createBell();

		bell0.setCenter_y(170);
		bell1.setCenter_y(130);
		bell2.setCenter_y(90);
		bell3.setCenter_y(50);
		bell4.setCenter_y(10);

		bell_list.removeAll(bell_list);
		bell_list.add(bell0);
		bell_list.add(bell1);
		bell_list.add(bell2);
		bell_list.add(bell3);
		bell_list.add(bell4);
	}

	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		bg.onDraw(canvas);
		
		if (!jump_button_clicked) {
			canvas.drawBitmap(bitmap_jump, 333, 199, paint);
		}
		
		drawBell(canvas);
		rabit.onDraw(canvas);
		drawScore(canvas);
		if (game_over) {
			conclusion.onDraw(canvas);
		}
		if (bird.isOn_screen()) {
			bird.onDraw(canvas);
		}
	}

	private void drawBell(Canvas canvas) {
		for (int i = 0; i < bell_list.size(); ++i) {
			bell_list.get(i).onDraw(canvas);
		}
	}

	private void drawScore(Canvas canvas) {
		paint.setColor(Color.WHITE);
		paint.setTextSize(15);
		canvas.drawText("" + score, 22, 22, paint);
	}


	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			this.refurbishThread.setGo_on(false);
		}
		return true;
	}

	public boolean onTouchEvent(MotionEvent event) {
		float x = event.getX();
		float y = event.getY();

		if (game_over) {
			////////////////////////////游戏结束后的触屏处理
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				if (x >= 165 && x <= 237 && y >= 154 && y <= 180) {
					conclusion.setPressed(true);
				}
				break;
			case MotionEvent.ACTION_MOVE:
				break;
			case MotionEvent.ACTION_UP:
				if (x >= 165 && x <= 237 && y >= 154 && y <= 180) {
					conclusion.setPressed(false);
					init();
					game_over = false;
				}
				break;
			default:
				break;
			}
		} else {
			////////////////////////////游戏中的触屏处理
			if (jump_button_clicked) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
				case MotionEvent.ACTION_MOVE:
				case MotionEvent.ACTION_UP:
					rabit.setX_destination(x);
					break;
				default:
					rabit.setX_destination(rabit.getX());
				}
			} else {
				if (x > 327 && x < 364 && y > 196 && y < 230) {
					if(event.getAction() == MotionEvent.ACTION_UP){
						jump_comm_flag = true;
						jump_button_clicked = true;
					}
				} else {
					switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
					case MotionEvent.ACTION_MOVE:
					case MotionEvent.ACTION_UP:
						rabit.setX_destination(x);
						break;
					default:
						rabit.setX_destination(rabit.getX());
					}
				}
			}
		}
		return true;
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
	}

	public void surfaceCreated(SurfaceHolder holder) {
		refurbishThread.setGo_on(true);
		refurbishThread.start();
		if (audio_on) {
			audioProvider.play_bg();
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		refurbishThread.setGo_on(false);
		if (audio_on) {
//			player.stop();
//			player.release();
//			soundPool.release();
			audioProvider.release();
		}
		// save highest score
		highest_score = (highest_score > score) ? highest_score : score;
		SharedPreferences pre = ((GameActivity) context).getPreferences(0);
		SharedPreferences.Editor editor = pre.edit();
		editor.putInt("highestscore", highest_score);
	}

	class RefurbishThread extends Thread {
		private boolean go_on = false;

		public boolean isGo_on() {
			return go_on;
		}

		public void setGo_on(boolean goOn) {
			go_on = goOn;
		}

		public void run() {
			while (go_on) {
				try {
					// Thread.sleep(50);
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				update_all_components();
				synchronized (holder) {
					Canvas canvas = holder.lockCanvas();
					GameSurfaceView.this.onDraw(canvas);
					holder.unlockCanvasAndPost(canvas);
				}
			}
		}
	}

	private void update_rabit(){
		/**
		 * rabit need jump up 93.72px per time 4 23 5 18 6 15
		 * */
	
		if (rabit.isOnGround()) {
			// 处理水平方向移动
			// rabit 到达目的位置
			if (Math.abs(rabit.getX() - rabit.getX_destination()) < 5) {
				if (rabit.isFaceLeft()) {
					rabit.setPic_state(Rabit.RABIT_PIC_LEFT_STOP);
					rabit.setFace_state(Rabit.RABIT_FACE_LEFT);
					rabit.setGround_state(Rabit.RABIT_LEFT_STOP);
				} else {
					rabit.setPic_state(Rabit.RABIT_PIC_RIGHT_STOP);
					rabit.setFace_state(Rabit.RABIT_FACE_RIGHT);
					rabit.setGround_state(Rabit.RABIT_RIGHT_STOP);
				}
			} else if (rabit.getX() - rabit.getX_destination() >= 5) {
				// rabit 在destination的右面，它将向左移动
				rabit.setFace_state(Rabit.RABIT_FACE_LEFT);
				rabit.setX(rabit.getX() - Rabit.SPEED_X);
				if (rabit.getGround_state() == Rabit.RABIT_LEFT_MOVE1_ON_GROUND) {
					rabit.setGround_state(Rabit.RABIT_LEFT_MOVE2_ON_GROUND);
					rabit.setPic_state(Rabit.RABIT_PIC_ON_GROUND_LEFT_JUMP1);
				} else if (rabit.getGround_state() == Rabit.RABIT_LEFT_MOVE2_ON_GROUND) {
					rabit.setGround_state(Rabit.RABIT_LEFT_MOVE1_ON_GROUND);
					rabit.setPic_state(Rabit.RABIT_PIC_ON_GROUND_LEFT_JUMP0);
				} else {
					rabit.setGround_state(Rabit.RABIT_LEFT_MOVE1_ON_GROUND);
					rabit.setPic_state(Rabit.RABIT_PIC_ON_GROUND_LEFT_JUMP0);
				}

			} else if (rabit.getX_destination() - rabit.getX() > 5) {
				// rabit 在destination的左面，它将向右移动
				rabit.setX(rabit.getX() + Rabit.SPEED_X);
				rabit.setFace_state(Rabit.RABIT_FACE_RIGHT);
				if (rabit.getGround_state() == Rabit.RABIT_RIGHT_MOVE1_ON_GROUND) {
					rabit.setGround_state(Rabit.RABIT_RIGHT_MOVE2_ON_GROUND);
					rabit.setPic_state(Rabit.RABIT_PIC_ON_GROUND_RIGHT_JUMP1);
				} else if (rabit.getGround_state() == Rabit.RABIT_RIGHT_MOVE2_ON_GROUND) {
					rabit.setGround_state(Rabit.RABIT_RIGHT_MOVE1_ON_GROUND);
					rabit.setPic_state(Rabit.RABIT_PIC_ON_GROUND_RIGHT_JUMP0);
				} else {
					rabit.setGround_state(Rabit.RABIT_RIGHT_MOVE1_ON_GROUND);
					rabit.setPic_state(Rabit.RABIT_PIC_ON_GROUND_RIGHT_JUMP0);
				}
			}
			// process jump command
			if (jump_comm_flag) {
				rabit.setAir_state(Rabit.RABIT_ON_AIR_UP0);
				rabit.setGround_state(Rabit.RABIT_NOT_ON_GROUND);
				rabit.setY(rabit.getY() - Rabit.SPEED_Y);
				if (rabit.isFaceLeft()) {
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_LEFT_JUMP);
				} else {
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_RIGHT_JUMP);
				}
				jump_comm_flag = false;
			}

		} else {
			// ////////////////////////////////////////////////////////////////////
			// rabit在空中
			// 处理水方向移动
			// 此处10值得商榷，很多定值需三思！！！！！！！！！！！！！
			if (rabit.getX() - rabit.getX_destination() >= 10) {
				// rabit 在destination的右面，它将向左移动
				rabit.setFace_state(Rabit.RABIT_FACE_LEFT);
				rabit.setX(rabit.getX() - Rabit.SPEED_X_ON_AIR);
			} else if (rabit.getX_destination() - rabit.getX() > 10) {
				// rabit 在destination的左面，它将向右移动
				rabit.setX(rabit.getX() + Rabit.SPEED_X_ON_AIR);
				rabit.setFace_state(Rabit.RABIT_FACE_RIGHT);
			}
			// 处理垂直方向 移动
			if (rabit.getAir_state() == Rabit.RABIT_ON_AIR_UP0) {
				// rabit.setY(rabit.getY()-Rabit.SPEED_Y);
				rabit_move_up();
				rabit.setAir_state(Rabit.RABIT_ON_AIR_UP1);
				if (rabit.isFaceLeft())
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_LEFT_JUMP);
				else
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_RIGHT_JUMP);
			} else if (rabit.getAir_state() == Rabit.RABIT_ON_AIR_UP1) {
				// rabit.setY(rabit.getY()-Rabit.SPEED_Y);
				rabit_move_up();
				rabit.setAir_state(Rabit.RABIT_ON_AIR_UP2);
				if (rabit.isFaceLeft())
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_LEFT_JUMP);
				else
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_RIGHT_JUMP);
			} else if (rabit.getAir_state() == Rabit.RABIT_ON_AIR_UP2) {
				// rabit.setY(rabit.getY()-Rabit.SPEED_Y);
				rabit_move_up();
				rabit.setAir_state(Rabit.RABIT_ON_AIR_UP3);
				if (rabit.isFaceLeft())
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_LEFT_JUMP);
				else
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_RIGHT_JUMP);
			} else if (rabit.getAir_state() == Rabit.RABIT_ON_AIR_UP3) {
				// rabit.setY(rabit.getY()-Rabit.SPEED_Y);
				rabit_move_up();
				rabit.setAir_state(Rabit.RABIT_ON_AIR_UP4);
				if (rabit.isFaceLeft())
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_LEFT_JUMP);
				else
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_RIGHT_JUMP);
			} else if (rabit.getAir_state() == Rabit.RABIT_ON_AIR_UP4) {
				// rabit.setY(rabit.getY()-Rabit.SPEED_Y);
				rabit_move_up();
				rabit.setAir_state(Rabit.RABIT_ON_AIR_UP5);
				if (rabit.isFaceLeft())
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_LEFT_JUMP);
				else
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_RIGHT_JUMP);
			} else if (rabit.getAir_state() == Rabit.RABIT_ON_AIR_UP5) {
				rabit.setAir_state(Rabit.RABIT_ON_AIR_STOP);
				if (rabit.isFaceLeft())
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_LEFT_STOP);
				else
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_RIGHT_STOP);
			} else if (rabit.getAir_state() == Rabit.RABIT_ON_AIR_STOP) {
				rabit.setAir_state(Rabit.RABIT_ON_AIR_DOWN);
				if (rabit.isFaceLeft())
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_LEFT_STOP);
				else
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_RIGHT_STOP);
			} else if (rabit.getAir_state() == Rabit.RABIT_ON_AIR_DOWN) {
				// rabit.setY(rabit.getY() + Rabit.SPEED_Y);
				rabit.setAir_state(Rabit.RABIT_ON_AIR_DOWN);
				if (rabit.isFaceLeft())
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_LEFT_DOWN);
				else
					rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_RIGHT_DOWN);
				rabit_move_down();
			}
		}
	}
	
	private void update_bells(){
		for (int i = 0; i < bell_list.size(); ++i) {
			Bell bell = bell_list.get(i);
			if (bell.isExplode()) {
				if (bell.getState() == Bell.BELL_EXPLODE0) {
					bell.setState(Bell.BELL_EXPLODE1);
				} else if (bell.getState() == Bell.BELL_EXPLODE1) {
					bell.setState(Bell.BELL_EXPLODE2);
				} else if (bell.getState() == Bell.BELL_EXPLODE2) {
					bell_list.remove(bell);
					bell_creator.recycle(bell);
					--i;
				}
			} else {
				if (rabit.isHitBell(bell)) {
					++hit_count;
					score += hit_count * 10;
					if(audio_on) audioProvider.play_bell_ding();
					bell.setState(Bell.BELL_EXPLODE0);
					rabit.setAir_state(Rabit.RABIT_ON_AIR_UP0);
					if (rabit.isFaceLeft()) {
						rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_LEFT_JUMP);
					} else {
						rabit.setPic_state(Rabit.RABIT_PIC_ON_AIR_RIGHT_JUMP);
					}
				}
			}
		}
	}
	
	private void update_bird(){
		if (bird.isOn_screen()) {
			// 处理垂直
			if (bird.getCenter_y() > Constant.SCREEN_HEIGHT) {
				bird.setOn_screen(false);
			}
		}
		if (bird.isOn_screen()) {
			// 处理水平
			if (bird.isHited()) {
				if (bird.isFaceLeft()) {
					bird
							.setCenter_x(bird.getCenter_x()
									- Bird.BIRD_SPEED_POWER);
					if (bird.getCenter_x() < -5) {
						bird.setOn_screen(false);
					}
				} else {
					bird
							.setCenter_x(bird.getCenter_x()
									+ Bird.BIRD_SPEED_POWER);
					if (bird.getCenter_x() > Constant.SCREEN_WIDTH + 5) {
						bird.setOn_screen(false);
					}
				}
			} else {
				if (bird.isFaceLeft()) {
					bird.setCenter_x(bird.getCenter_x() - Bird.BIRD_SPEED);
					if (bird.getCenter_x() < -5) {
						bird.setCenter_x(0);
						bird.setState(Bird.BIRD_RIGHT_FLY0);
					} else {
						if (rabit.isHitBird(bird)) {
							++hit_count;
							score *= 2;
							if(audio_on) audioProvider.play_twitter();
							bird.setState(Bird.BIRD_LEFT_FLY_POWER);
							rabit.setAir_state(Rabit.RABIT_ON_AIR_UP0);
							if (rabit.isFaceLeft()) {
								rabit
										.setPic_state(Rabit.RABIT_PIC_ON_AIR_LEFT_JUMP);
							} else {
								rabit
										.setPic_state(Rabit.RABIT_PIC_ON_AIR_RIGHT_JUMP);
							}
						} else if (bird.getState() == Bird.BIRD_LEFT_FLY0) {
							bird.setState(Bird.BIRD_LEFT_FLY1);
						} else if (bird.getState() == Bird.BIRD_LEFT_FLY1) {
							bird.setState(Bird.BIRD_LEFT_FLY2);
						} else {
							bird.setState(Bird.BIRD_LEFT_FLY0);
						}
					}

				} else {
					bird.setCenter_x(bird.getCenter_x() + Bird.BIRD_SPEED);
					if (bird.getCenter_x() > Constant.SCREEN_WIDTH + 5) {
						bird.setCenter_x(Constant.SCREEN_WIDTH);
						bird.setState(Bird.BIRD_LEFT_FLY0);
					} else {
						if (rabit.isHitBird(bird)) {
							++hit_count;
							score *= 2;
							if(audio_on) audioProvider.play_twitter();
							bird.setState(Bird.BIRD_LEFT_FLY_POWER);
							rabit.setAir_state(Rabit.RABIT_ON_AIR_UP0);
							if (rabit.isFaceLeft()) {
								rabit
										.setPic_state(Rabit.RABIT_PIC_ON_AIR_LEFT_JUMP);
							} else {
								rabit
										.setPic_state(Rabit.RABIT_PIC_ON_AIR_RIGHT_JUMP);
							}
						} else if (bird.getState() == Bird.BIRD_RIGHT_FLY0) {
							bird.setState(Bird.BIRD_RIGHT_FLY1);
						} else if (bird.getState() == Bird.BIRD_RIGHT_FLY1) {
							bird.setState(Bird.BIRD_RIGHT_FLY2);
						} else {
							bird.setState(Bird.BIRD_RIGHT_FLY0);
						}
					}
				}
			}
		}
		// 添加一个bird的逻辑
		if (hit_count % 11 == 0 && bird.isOn_screen() == false
				&& rabit.getY() < 210) {
			init_bird();
		}
	}
	
	private void update_all_components() {
		if (game_over)
			return;
		update_rabit();
		update_bells();
		update_bird();
	}

	private void rabit_move_up() {
		if (rabit.getCenter_y() > Constant.SCREEN_HEIGHT / 2) {
			rabit.setY(rabit.getY() - Rabit.SPEED_Y);
			return;
		}

		// 处理屏幕背景的变化
		// 处理bells的位置
		// 所有的bell下移
		
		// rabit跳跃至屏幕1/2处~2/3时，处理整个场景Components变化
		if (rabit.getCenter_y() > 0.33 * Constant.SCREEN_HEIGHT) {
			for (int i = 0; i < bell_list.size(); ++i) {
				Bell bell = bell_list.get(i);
				bell.setCenter_y(bell.getCenter_y() + Rabit.SPEED_Y / 2);
			}
			bg.drag_down((int) Rabit.SPEED_Y / 2);
			bird.setCenter_y(bird.getCenter_y() + Rabit.SPEED_Y / 2);
			rabit.setCenter_y(rabit.getCenter_y() - Rabit.SPEED_Y / 2);
		} else {
			// rabit跳跃比屏幕的2/3处还高时，处理整个场景的变化
			for (int i = 0; i < bell_list.size(); ++i) {
				Bell bell = bell_list.get(i);
				bell.setCenter_y(bell.getCenter_y() + Rabit.SPEED_Y);
			}
			bg.drag_down((int) Rabit.SPEED_Y);
			if (bird.isOn_screen()) {
				bird.setCenter_y(bird.getCenter_y() + Rabit.SPEED_Y);
			}
		}
		// 是否需要清理落地的bell, bell_list[0] 处于最底处，判断它是否落地
		Bell lowBell = bell_list.get(0);
		if (lowBell != null) {
			if (lowBell.getCenter_y() > 230) {
				bell_list.remove(lowBell);
				bell_creator.recycle(lowBell);
			}
		}
		// 判断是否需要添加新的bell， bell_list[size-1] 处于最高处，通过它来判断
		if (bell_list.size() > 0) {
			Bell upBell = bell_list.get(bell_list.size() - 1);
			if (upBell.getCenter_y() > 50)
				bell_list.add(bell_creator.createBell());
		}

	}

	private void rabit_move_down() {
		if (rabit.getY() > 210) {
			//rabit已经下落至屏幕底部，不可能在碰到bell， rabit输了高空下坠时背景，bell，bird的位置处理
			if (bg.isLowest()) {
				game_over = true;
				rabit.setY(Constant.RABIT_INIT_Y);
				if (rabit.isFaceLeft()) {
					rabit.setPic_state(Rabit.RABIT_PIC_LEFT_STOP);
				} else {
					rabit.setPic_state(Rabit.RABIT_PIC_RIGHT_STOP);
				}
				bg.init();
			} else {
				// bg 向上拖动
				bg.drag_up((int) Rabit.SPEED_Y);
				//屏幕上的bird向上移动
				if (bird.isOn_screen()) {
					bird.setCenter_y(bird.getCenter_y() - Rabit.SPEED_Y);
					if (bird.getCenter_y() < 0) {
						bird.setOn_screen(false);
					}
				}
				// 所有的bell向上移动
				for (int i = 0; i < bell_list.size(); ++i) {
					Bell bell = bell_list.get(i);
					bell.setCenter_y(bell.getCenter_y() - Rabit.SPEED_Y);
				}
				// 处理移动到屏幕外的bell
				if (bell_list.size() > 0) {
					Bell bell_up = bell_list.get(bell_list.size() - 1);
					if (bell_up.getCenter_y() < -Bell.BELL_OK_HEIGHT / 2) {
						bell_list.remove(bell_up);
						bell_creator.recycle(bell_up);
					}
				}
			}
		} else {
			// rabit 还没有玩完
			rabit.setY(rabit.getY() + Rabit.SPEED_Y);
		}
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getHighest_score() {
		return highest_score;
	}

	public void setHighest_score(int highestScore) {
		highest_score = highestScore;
	}

}
