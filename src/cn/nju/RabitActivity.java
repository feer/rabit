package cn.nju;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class RabitActivity extends Activity {
	private IntroduceView introduceView;
	private HelpView helpView;
	private AudioView audioView;
	private View currentView;
	private boolean audio_on = false;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		introduceView = new IntroduceView(this);
		helpView = new HelpView(this);
		audioView = new AudioView(this);
		initWindow();
		setContentView(introduceView);
		this.currentView = introduceView;
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		this.finish();
	}

	private void initWindow() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// 获得屏幕宽高
		//		
		DisplayMetrics dm = new DisplayMetrics();
		this.getWindowManager().getDefaultDisplay().getMetrics(dm);
		int width = dm.widthPixels;
		int height = dm.heightPixels;
		System.out.println("width " + width);
		System.out.println("height " + height);
		/*
		 * 另一种方法 int screenWidth,screenHeight; WindowManager windowManager =
		 * getWindowManager(); Display display =
		 * windowManager.getDefaultDisplay(); screenWidth = display.getWidth();
		 * screenHeight = display.getHeight();
		 */
	}

	public void toGameActivity() {
		Intent intent = new Intent();
		intent.setClass(this, GameActivity.class);
		intent.putExtra("audio", audio_on);
		this.startActivity(intent);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		System.out.println("screen touched");
		if (currentView == introduceView) {
			if (event.getAction() == MotionEvent.ACTION_UP) {
				this.setContentView(helpView);
				this.currentView = helpView;
			}
		} else if (currentView == helpView) {
			if (event.getAction() == MotionEvent.ACTION_UP) {
				this.setContentView(audioView);
				this.currentView = audioView;
			}
		} else {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				// 33 197 336 197
				float x = event.getX();
				float y = event.getY();
				System.out.println("x->" + x);
				System.out.println("y->" + y);
				if (x > 30 && x < 60 && y > 177 && y < 207) {
					System.out.println("yes");
					audioView.setClick(true);
					audioView.setAudio_on(true);
					audioView.invalidate();
					this.audio_on = true;
				}
				if (x > 330 && x < 360 && y > 177 && y < 207) {
					System.out.println("no");
					audioView.setClick(true);
					audioView.setAudio_on(false);
					audioView.invalidate();
					this.audio_on = false;
				}
				break;
			case MotionEvent.ACTION_UP:
				if (audioView.isClick()) {
					this.toGameActivity();
				}
				break;
			}
		}
		return true;
	}

}